package challenges;

public class SwitchExpression {
	public static void main(String[] args) 
		
	
	{
	      int a=1,b=1;

	      switch(a+b)   //Used expression 'a+b' instead of simple variable 'a' here
	      {

	        case 0:
	           System.out.println("The value of a+b is zero");
	           break;
	        case 1:
	           System.out.println("The value of a+b is one");
	           break;
	        case 2:
	           System.out.println("The value of a+b is two");
	           break;
	        default:
	           System.out.println("The value of a+b is "+(a+b));

	      }
	   }

}

