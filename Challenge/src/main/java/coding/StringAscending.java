package coding;

import java.util.Arrays;

public class StringAscending {

	public static void main(String[] args) {

		String str = "my name is bharath";
		char ch[] = str.toCharArray();
		Arrays.sort(ch);
		String sortedStr = new String(ch);
		System.out.println(sortedStr);
		
	}

}
